import os
import hashlib
from flask import Flask, request, redirect, url_for, send_from_directory, flash
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = '/tmp'
# Not required, but would seem a likely feature in the future
# ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# Configures the application configuration, the file size is limited to
# 64 MB controlled by MAX_CONTENT_LENGTH
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 64 * 1024 * 1024

'''
# Function to check whether the file extention is allowed not required
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
'''

# Function returns the hash of the filename passed to it
# sha2 should be used as md5 is cryptographically unsecure
def md5_checksum(filename, block_size=65536):
    # sha256 = hashlib.sha256()
    md5 = hashlib.md5()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            md5.update(block)
    return md5.hexdigest()

@app.route('/uploads/<filename>')
def uploaded_file_hash(filename):
    fqfilename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    header = '''
    <!doctype html>
    <title>File Uploaded</title>
    <html>
        <h1>File Uploaded, the md5 hash is </h1>
        
        <p>
    '''
    page = header + md5_checksum(fqfilename) + '''
        </p>
    </html>
    '''

    # Clear up the tmp directory
    os.remove(fqfilename)
    
    return page


# The root of the server application
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if the user does not select a file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file:
            filename = secure_filename(file.filename)
            fqfilename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(fqfilename)
            # sha256_checksum(fqfilename)
            return redirect(url_for('uploaded_file_hash',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload New File</title>
    <html>
        <h1>Upload New File to Find the Hash</h1>
        <form method=post enctype=multipart/form-data>
        <p><input type=file name=file>
            <input type=submit value=Upload>
        </form>
    </html>
    '''

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)    
