FROM alpine

ARG BUILD_DATE
ARG VCS_REF

LABEL version="0.1"
LABEL description="Docker file for the server upload application"
LABEL Maintainer="Oliver Sanders" 
LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-ref=$VCS_REF

# Create the the source directory
RUN mkdir -p /usr/src/app

# Add Python and the pre-reqs to the image
RUN apk update
RUN apk add --no-cache \
    python \
    python-dev \
    py-pip \
    build-base

COPY requirements.txt /usr/src/app/
RUN pip install --upgrade pip

WORKDIR /usr/src/app
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# Expose the Flask port
EXPOSE 5000

CMD [ "python", "/usr/src/app/app.py" ]
