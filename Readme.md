# Client Server File Upload

## Description

This repository holds the code for the client and server software. Once the server has been built as per the build instructions; a user will be able to upload a file using a client and in return will get a md5 hash of the uploaded file.

## Limitations

## Build Process

This sections steps through the actions to enable you to get the source for the exercise and build the client and server. This build process assumes you have root or system administrator level privileges which allow you to install software to the build machine

### Install Docker

Ubuntu [Docker Installation instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/ "Docker Installation Ubuntu")

Windows [Docker Installation instructions](https://docs.docker.com/docker-for-windows/install/ "Docker Installation Windows")

### Optional: Install Git

For Ubuntu run the following command:

```sudo apt install git-all```

For Windows the software can be downloaded from [Git Software](http://git-scm.com/download/win "Windows Git Software"), there is also a Chocolatey package (this is community maintained)

### Optional: Clone Repository

1. Navigate to a folder where you want to keep the development files.
1. Run the following git command to clone the source to your local directory:

```git clone https://gitlab.com/orsanders/clientserverupload.git```

## Running the service

Once you have completed the Build process you are now in the position to run the client and server applications. First we will start the server process then we will use the client to test and prove the application.

### Server

Generate docker image:

### Create Docker Image

```docker build -t orsande/flask-fileupload```

### Optional: Push the docker image to a registry

## Using the service

The following command will start a docker container and map a port (5000) to the host. Note you can use the "-d" option to run the container in detached mode.

```docker run --rm -p 5000:5000 --name=server-fileupload orsande/flask-fileupload```

### Client

Any web browser or command line tool that can communicate using the http protocol is suitable. The url will be [File Upload/Hash Service](http://0.0.0.0:5000/ "File Upload/Hash Service")
